import serial.tools.list_ports
from views.Dashboard import Dashboard
from src.devices.SunScreenDevice import SunScreenDevice
from src.managers.DeviceManager import DeviceManager
from views import Authenticate, AbstractView


class Bootstrap:

    deviceManager: DeviceManager
    view: AbstractView.AbstractView

    def __init__(self):
        self.deviceManager = DeviceManager(self.get_device_list())

    def get_device_list(self):
        comPorts = list(serial.tools.list_ports.comports())
        devices = []

        for comPort in comPorts:
            if comPort.vid == 9025 and comPort.pid == 67:
                print(comPort.device)
                devices.append(SunScreenDevice(comPort.device))

        return devices

    def switch_view(self, view):
        self.view.destroy()
        self.view = view
        self.view.build()

    def run(self):
        self.view = Authenticate.Authenticate(lambda login: self.switch_view(Dashboard(self, self.deviceManager, login)))
        self.view.build()

app = Bootstrap()
app.run()
