from src.devices.SunScreenDevice import SunScreenDevice
from views.AbstractView import AbstractView
from views.DeviceEdit import DeviceEdit
from views.DeviceChart import DeviceChart
from tkinter import *
import time

class DeviceDetail(AbstractView):

    device: SunScreenDevice

    def __init__(self, device: SunScreenDevice):
        self.device = device

    def build(self, title="Device Detail"):
        super().build(title + ": " + self.device.device_name)

        Label(self.view, text="Status").grid(row=1, column=0)
        self.label_status = Label(self.view, text=self.device.status_ok and 'Goed' or 'Fout', fg=self.device.status_ok and 'green' or 'red')
        self.label_status.grid(row=1, column=1)

        Label(self.view, text="Temperatuur").grid(row=2, column=0)
        self.label_temp = Label(self.view, text=str(self.device.get_temperature()) + " (℃)")
        self.label_temp.grid(row=2, column=1)

        Label(self.view, text="Lichtwaarde").grid(row=3, column=0)
        self.label_light = Label(self.view, text=self.device.get_light())
        self.label_light.grid(row=3, column=1)

        Label(self.view, text="Scherm op automatisch").grid(row=4, column=0)
        Button(self.view, text="Automatisch", command=lambda: self.device.set_mode_automatic(), width=10).grid(row=4, column=1)
        Button(self.view, text="Handmatig", command=lambda: self.device.set_mode_manual(), width=10).grid(row=4, column=2)

        Label(self.view, text="Scherm naar beneden/omhoog").grid(row=5, column=0)
        Button(self.view, text="Beneden", command=lambda: self.device.panel_out(), width=10).grid(row=5, column=1)
        Button(self.view, text="Omhoog", command=lambda: self.device.panel_in(), width=10).grid(row=5, column=2)

        Button(self.view, text="Chart", command=lambda: self.openChartWindow(), width=10).grid(row=6, column=1)
        Button(self.view, text="Configureer", command=lambda: self.openConfigureWindow(), width=10).grid(row=6, column=2)

        self.center()
        self.view.update_idletasks()
        self.view.update()

        self.update()

    def update(self):
        self.label_status.config(text=self.device.status_ok and 'Goed' or 'Fout', fg=self.device.status_ok and 'green' or 'red')
        time.sleep(.2)
        self.label_temp.config(text=str(self.device.get_temperature()) + " (℃)")
        time.sleep(.2)
        self.label_light.config(text=self.device.get_light())

        self.view.update_idletasks()
        self.view.update()
        self.view.after(5000, self.update)

    def openChartWindow(self):
        chart_view = DeviceChart(self.device)
        chart_view.build()

    def openConfigureWindow(self):
        configure_view = DeviceEdit(self.device)
        configure_view.build()
