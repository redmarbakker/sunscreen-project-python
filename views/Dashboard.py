from src.managers.DeviceManager import DeviceManager
from src.devices.SunScreenDevice import SunScreenDevice
from views.DeviceDetail import DeviceDetail
from views.AbstractView import AbstractView
from typing import List
from tkinter import *
import time


class Dashboard(AbstractView):

    bootstrap = False
    device_manager: DeviceManager
    device_detail_views: List[DeviceDetail]
    user_name: str
    device_list = {}

    def __init__(self, bootstrap, device_manager: DeviceManager, login: str):
        self.device_manager = device_manager
        self.bootstrap = bootstrap
        self.user_name = login

    def build(self, title="Dashboard"):
        super().build(title + " (user: " + self.user_name + ")")
        self.view.minsize(500, 180)
        self.view.resizable(0, 0)

        #  Master label
        Label(self.view, text="Aangesloten devices").grid(row=0, column=0)

        #  Algemene instellingen
        Label(self.view, text="Alle schermen naar beneden").grid(row=0, column=3)
        Button(self.view, text="Omhoog", command=lambda: self.pannels_in(), width=15).grid(row=0, column=4)
        Button(self.view, text="Omlaag", command=lambda: self.pannels_out(), width=15).grid(row=0, column=5)

        Label(self.view, text="Alle schermen op automatisch", justify=LEFT).grid(row=1, column=3)
        Button(self.view, text="Automatisch", command=lambda: self.pannels_automatic(), width=15).grid(row=1, column=4)
        Button(self.view, text="Handmatig", command=lambda: self.pannels_manual(), width=15).grid(row=1, column=5)

        Label(self.view, text="Temperatuur", justify=LEFT).grid(row=2, column=3)
        self.label_temp = Label(self.view, text=str(self.avg_temperature()) + " (℃)")
        self.label_temp.grid(row=2, column=5)

        Label(self.view, text="Algemene status").grid(row=3, column=3)
        self.label_status = Label(self.view, text=self.get_status() and 'Goed' or 'Fout', fg=self.get_status() and 'green' or 'red')
        self.label_status.grid(row=3, column=5)

        self.create_device_list()

        # Button(self.view, text="Refresh", command=lambda: self.refresh_device_list(), width=15).grid(row=t, column=0)

        self.center()

        self.view.update_idletasks()
        self.view.update()

        self.update()

    def update(self):
        self.label_temp.config(text=str(self.avg_temperature()) + " (℃)")
        time.sleep(.2)
        self.label_status.config(text=self.get_status() and 'Goed' or 'Fout', fg=self.get_status() and 'green' or 'red')
        time.sleep(.2)
        self.refresh_device_list()

        self.view.update_idletasks()
        self.view.update()
        self.view.after(5000, self.update)

    def get_status(self):
        for device in self.device_manager.get_all():
            if device.status_ok is False:
                return False

        return True

    def open_detail(self, device: SunScreenDevice):
        device_detail_view = DeviceDetail(device)
        device_detail_view.build()

    def create_device_list(self):
        t = 1
        for device in self.device_manager.get_all():
            self.device_list[t] = Button(
                self.view,
                text=device.device_name,
                command=lambda: self.open_detail(device),
                width=15
            )
            self.device_list[t].grid(row=t, column=0)
            t += 1

    def refresh_device_list(self):
        device_list = self.bootstrap.get_device_list()

        for device in device_list:
            for known_device in self.device_manager.get_all():
                if str(device.port) == str(known_device.port):
                    return

            self.device_manager.add(device)

        for key, device_elem in self.device_list.items():
            device_elem.destroy()

        self.device_list = {}
        self.create_device_list()

    def pannels_in(self):
        for device in self.device_manager.get_all():
            device.panel_in()

    def pannels_out(self):
        for device in self.device_manager.get_all():
            device.panel_out()

    def pannels_automatic(self):
        for device in self.device_manager.get_all():
            device.set_mode_automatic()

    def pannels_manual(self):
        for device in self.device_manager.get_all():
            device.set_mode_manual()

    def avg_temperature(self):
        devices = self.device_manager.get_all()
        temp: int = 0
        for device in devices:
            temp = int(temp) + device.get_temperature()

        if len(devices) is 0:
            return 0

        return temp / len(devices)