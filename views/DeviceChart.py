import matplotlib.pyplot as plt
from views.AbstractView import AbstractView
from src.devices.SunScreenDevice import SunScreenDevice
import datetime


class DeviceChart(AbstractView):
    x = ['10:45', '10:50', '10:55', '11:00', '11:05', '11:10', '11:15']
    y = [19, 19, 18, 19, 20, 20, 21]

    def __init__(self, device: SunScreenDevice):
        self.device = device

    def build(self, title="Device Chart"):
        self.draw(self.x, self.y)

    def update(self):
        plt.plot(datetime.datetime.now().time(), self.device.get_temperature())

        self.view.update_idletasks()
        self.view.update()
        self.view.after(5000, self.update)

    def draw(self, x: str, y: int):
        plt.plot(x, y)
        plt.ylabel('Temperatuur')
        plt.xlabel('Tijd')
        plt.show()