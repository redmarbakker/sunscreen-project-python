from tkinter import *


class AbstractView:

    view: Tk

    def build(self, title):
        self.view = Tk()
        self.view.title(title)

    def center(self):
        self.view.update_idletasks()

        screen_width = self.view.winfo_screenwidth()
        screen_height = self.view.winfo_screenheight()

        size = tuple(int(_) for _ in self.view.geometry().split('+')[0].split('x'))
        x = screen_width / 2 - size[0] / 2
        y = screen_height / 3 - size[1] / 2

        self.view.geometry("+%d+%d" % (x, y))

    def destroy(self):
        self.view.destroy()
