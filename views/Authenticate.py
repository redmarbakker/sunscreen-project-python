import tkinter.messagebox
from tkinter import *
from typing import Callable
from views.AbstractView import AbstractView


class Authenticate(AbstractView):

    auth_callback: Callable

    pass_label: Label
    pass_entry: Entry
    auth_button: Button

    def __init__(self, auth_callback: Callable):
        self.auth_callback = auth_callback

    def build(self, title="Login"):
        super().build(title)

        self.view.resizable(0, 0)
        self.view.winfo_toplevel().title = "Login"

        self.user_label = Label(self.view, text="Username")
        self.user_label.grid(row=0, column=0)
        self.user_entry = Entry(self.view, bd=5)
        self.user_entry.grid(row=0, column=1)

        self.pass_label = Label(self.view, text="Password")
        self.pass_label.grid(row=1, column=0)
        self.pass_entry = Entry(self.view, bd=5, show="*")
        self.pass_entry.grid(row=1, column=1)

        self.auth_button = Button(self.view, text="Login", command=self.auth, width=20)
        self.auth_button.grid(row=2, column=1)

        self.center()
        self.view.mainloop()

    def auth(self):
        lines = [line.rstrip('\n') for line in open('./data/password.txt')]

        for allowed_login in lines:
            login, password = allowed_login.split(':')

            if login == self.user_entry.get() and password == self.pass_entry.get():
                return self.auth_callback(login)

        tkinter.messagebox.showinfo("Oops", "Wrong pass...")