from src.devices.SunScreenDevice import SunScreenDevice
from views.AbstractView import AbstractView
from tkinter import *

class DeviceEdit(AbstractView):

    device: SunScreenDevice

    device_name: Entry
    max_distance: Entry
    temperature_threshold_minimum: Entry
    temperature_threshold_maximum: Entry
    light_threshold_minimum: Entry
    light_threshold_maximum: Entry

    def __init__(self, device: SunScreenDevice):
        self.device = device

    def build(self, title="Device Edit"):
        super().build(title + ": " + self.device.device_name)

        Label(self.view, text="Naam").grid(row=0, column=0)
        self.device_name = Entry(
            self.view,
            bd=5,
            textvariable=StringVar(
                self.view,
                value=self.device.device_name
            )
        )
        self.device_name.grid(row=0, column=1)

        Label(self.view, text="Max uitrol afstand").grid(row=1, column=0)
        self.max_distance = Entry(
            self.view,
            bd=5,
            textvariable=StringVar(
                self.view,
                value=self.device.max_distance
            )
        )
        self.max_distance.grid(row=1, column=1)

        Label(self.view, text="Minimale waarde temperatuur").grid(row=2, column=0)
        self.temperature_threshold_minimum = Entry(
            self.view,
            bd=5,
            textvariable=StringVar(
                self.view,
                value=self.device.temperature_threshold_minimum
            )
        )
        self.temperature_threshold_minimum.grid(row=2, column=1)

        Label(self.view, text="Maximale waarde temperatuur").grid(row=3, column=0)
        self.temperature_threshold_maximum = Entry(
            self.view,
            bd=5,
            textvariable=StringVar(
                self.view,
                value=self.device.temperature_threshold_maximum
            )
        )
        self.temperature_threshold_maximum.grid(row=3, column=1)

        Label(self.view, text="Minimale waarde licht").grid(row=4, column=0)
        self.light_threshold_minimum = Entry(
            self.view,
            bd=5,
            textvariable=StringVar(
                self.view,
                value=self.device.light_threshold_minimum
            )
        )
        self.light_threshold_minimum.grid(row=4, column=1)

        Label(self.view, text="Maximale waarde light").grid(row=5, column=0)
        self.light_threshold_maximum = Entry(
            self.view,
            bd=5,
            textvariable=StringVar(
                self.view,
                value=self.device.light_threshold_maximum
            )
        )
        self.light_threshold_maximum.grid(row=5, column=1)

        Button(self.view, text="Opslaan", command=lambda: self.save(), width=20).grid(row=6, column=1)

        self.center()
        self.view.update_idletasks()
        self.view.update()

    def save(self):
        valid = True

        if str(self.device_name.get()) is not str(self.device.device_name):
            self.device.set_device_name(self.device_name.get())

        if int(self.max_distance.get()) is not int(self.device.max_distance):
            self.device.set_max_distance(self.max_distance.get())

        if int(self.temperature_threshold_minimum.get()) < int(self.temperature_threshold_maximum.get()) | 0:
            self.temperature_threshold_minimum.config(bg="white")
            self.temperature_threshold_maximum.config(bg="white")

            if int(self.temperature_threshold_minimum.get()) is not int(self.device.temperature_threshold_minimum) | 0:
                self.device.set_temperature_threshold_minimum(self.temperature_threshold_minimum.get())

            if int(self.temperature_threshold_maximum.get()) is not int(self.device.temperature_threshold_maximum) | 0:
                self.device.set_temperature_threshold_maximum(self.temperature_threshold_maximum.get())
        else:
            valid = False
            self.temperature_threshold_minimum.config(bg="red")
            self.temperature_threshold_maximum.config(bg="red")

        if int(self.light_threshold_minimum.get()) < int(self.light_threshold_maximum.get()) | 0:
            self.light_threshold_minimum.config(bg="white")
            self.light_threshold_maximum.config(bg="white")

            if int(self.light_threshold_minimum.get()) is not int(self.device.light_threshold_minimum) | 0:
                self.device.set_light_threshold_minimum(self.light_threshold_minimum.get())

            if int(self.light_threshold_maximum.get()) is not int(self.device.light_threshold_maximum) | 0:
                self.device.set_light_threshold_maximum(self.light_threshold_maximum.get())
        else:
            valid = False
            self.light_threshold_minimum.config(bg="red")
            self.light_threshold_maximum.config(bg="red")

        if valid is True:
            self.view.destroy()