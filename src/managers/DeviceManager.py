from src.devices.SunScreenDevice import SunScreenDevice
from typing import List


class DeviceManager:

    devices: List[SunScreenDevice] = []

    def __init__(self, devices: List[SunScreenDevice]=[]):
        if len(devices) < 1:
            self.devices = list()
        else:
            self.devices = []
            for device in devices:
                self.add(device)

    def add(self, device: SunScreenDevice):
        device.connect()
        device.handshake()
        self.devices.append(device)

    def get(self, name):
        for device in self.devices:
            if device.device_name == name:
                return device

    def get_all(self) -> List[SunScreenDevice]:
        return self.devices
