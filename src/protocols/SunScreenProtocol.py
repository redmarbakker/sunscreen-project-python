class SunScreenSendProtocol:

    handshake = 'handshake'

    get_temperature = 'get_temperature'
    get_light = 'get_light'
    get_distance = 'get_distance'

    set_device_name = 'set_device_name:%s'
    set_max_distance = 'set_max_distance:%s'

    panel_out = 'panel_out'
    panel_in = 'panel_in'

    set_light_threshold_minimum = 'set_light_threshold_minimum:%s'
    set_light_threshold_maximum = 'set_light_threshold_maximum:%s'

    set_temperature_threshold_minimum = 'set_temperature_threshold_minimum:%s'
    set_temperature_threshold_maximum = 'set_temperature_threshold_maximum:%s'

    set_mode_automatic = 'set_mode_automatic'
    set_mode_manual = 'set_mode_manual'


class SunScreenReceiveProtocol:

    handshake = {
        2: [
            {'name': 'identifier'},
            {'name': 'device_name'},
            {'name': 'device_mode'},
            {'name': 'temperature_threshold_minimum'},
            {'name': 'temperature_threshold_maximum'},
            {'name': 'light_threshold_minimum'},
            {'name': 'light_threshold_maximum'},
            {'name': 'max_distance'},
        ],
        4: []
    }

    get_temperature = {
        2: [{'name': 'temperature'}],
        4: []
    }
    get_light = {
        2: [{'name': 'light_intensity'}],
        4: []
    }
    get_distance = {
        2: [{'name': 'distance'}],
        4: []
    }

    set_device_name = {
        2: [],
        4: []
    }
    set_max_distance = {
        2: [],
        4: []
    }

    panel_out = {
        2: [],
        4: []
    }
    panel_in = {
        2: [],
        4: []
    }

    set_light_threshold_minimum = {
        2: [],
        4: []
    }
    set_light_threshold_maximum = {
        2: [],
        4: []
    }

    set_temperature_threshold_minimum = {
        2: [],
        4: []
    }
    set_temperature_threshold_maximum = {
        2: [],
        4: []
    }

    set_mode_automatic = {
        2: [],
        4: []
    }
    set_mode_manual = {
        2: [],
        4: []
    }