from src.protocols import SunScreenProtocol
from src.responses.SunScreenResponse import SunScreenResponse
from src.service.SerialService import SerialService


class SunScreenDevice:

    serial_service = False

    send_protocol = SunScreenProtocol.SunScreenSendProtocol
    receive_protocol = SunScreenProtocol.SunScreenReceiveProtocol

    port: str

    status_ok = True

    identifier: str = ''
    device_name: str = ''
    device_mode: str = ''
    temperature_threshold_maximum: str = ''
    temperature_threshold_minimum: str = ''
    light_threshold_maximum: str = ''
    light_threshold_minimum: str = ''
    max_distance: str = ''

    def __init__(self, port):
        self.port = port
        self.serial_service = SerialService()

    def connect(self):
        self.serial_service.connect(self.port)

    def get_temperature(self):
        try:
            response = SunScreenResponse(
                protocol=self.receive_protocol.get_temperature,
                response=self.serial_service.request(self.send_protocol.get_temperature)
            )

            if response.get_response_code() > 2:
                self.status_ok = False
                return 0

            return int(response.get_data()['temperature'])
        except Exception as e:
            print(e)
            self.status_ok = False
            return 0

    def get_light(self):
        try:
            response = SunScreenResponse(
                protocol=self.receive_protocol.get_light,
                response=self.serial_service.request(self.send_protocol.get_light)
            )

            return response.get_data()['light_intensity']
        except Exception as e:
            print('Ex on get_light')
            print(e)
            self.status_ok = False
            return 0

    def get_distance(self):
        try:
            response = SunScreenResponse(
                protocol=self.receive_protocol.get_distance,
                response=self.serial_service.request(self.send_protocol.get_distance)
            )

            return response.get_data()['distance']
        except Exception as e:
            print(e)
            self.status_ok = False
            return 0

    def panel_out(self):
        try:
            response = SunScreenResponse(
                protocol=self.receive_protocol.panel_out,
                response=self.serial_service.request(self.send_protocol.panel_out)
            )

            return response.get_data()
        except Exception as e:
            print(e)
            self.status_ok = False

    def panel_in(self):
        try:
            response = SunScreenResponse(
                protocol=self.receive_protocol.panel_in,
                response=self.serial_service.request(self.send_protocol.panel_in)
            )

            return response.get_data()
        except Exception as e:
            print(e)
            self.status_ok = False

    def set_light_threshold_minimum(self, minimum: str):
        try:
            response = SunScreenResponse(
                protocol=self.receive_protocol.set_light_threshold_minimum,
                response=self.serial_service.request(self.send_protocol.set_light_threshold_minimum, [minimum])
            )

            self.light_threshold_minimum = minimum

            return response.get_data()
        except Exception as e:
            print(e)
            self.status_ok = False

    def set_light_threshold_maximum(self, maximum: str):
        try:
            response = SunScreenResponse(
                protocol=self.receive_protocol.set_light_threshold_maximum,
                response=self.serial_service.request(self.send_protocol.set_light_threshold_maximum, [maximum])
            )

            self.light_threshold_maximum = maximum

            return response.get_data()
        except Exception as e:
            print(e)
            self.status_ok = False

    def set_temperature_threshold_minimum(self, minimum: str):
        try:
            response = SunScreenResponse(
                protocol=self.receive_protocol.set_temperature_threshold_minimum,
                response=self.serial_service.request(self.send_protocol.set_temperature_threshold_minimum, [minimum])
            )

            self.temperature_threshold_minimum = minimum

            return response.get_data()
        except Exception as e:
            print(e)
            self.status_ok = False

    def set_temperature_threshold_maximum(self, maximum: str):
        try:
            response = SunScreenResponse(
                protocol=self.receive_protocol.set_temperature_threshold_maximum,
                response=self.serial_service.request(self.send_protocol.set_temperature_threshold_maximum, [maximum])
            )

            self.temperature_threshold_maximum = maximum

            return response.get_data()
        except Exception as e:
            print(e)
            self.status_ok = False

    def set_max_distance(self, distance):
        try:
            response = SunScreenResponse(
                protocol=self.receive_protocol.set_max_distance,
                response=self.serial_service.request(self.send_protocol.set_max_distance, [distance])
            )

            self.max_distance = distance

            return response.get_data()
        except Exception as e:
            print(e)
            self.status_ok = False

    def handshake(self):
        try:
            response = SunScreenResponse(
                protocol=self.receive_protocol.handshake,
                response=self.serial_service.request(self.send_protocol.handshake)
            )

            data = response.get_data()

            self.identifier = data['identifier']
            self.device_name = data['device_name']
            self.device_mode = data['device_mode']
            self.temperature_threshold_minimum = data['temperature_threshold_minimum']
            self.temperature_threshold_maximum = data['temperature_threshold_maximum']
            self.light_threshold_minimum = data['light_threshold_minimum']
            self.light_threshold_maximum = data['light_threshold_maximum']
            self.max_distance = data['max_distance']

            return data
        except Exception as e:
            self.status_ok = False
            print('Ex on handshake')
            print(e)

    def set_mode_automatic(self):
        try:
            response = SunScreenResponse(
                protocol=self.receive_protocol.set_mode_automatic,
                response=self.serial_service.request(self.send_protocol.set_mode_automatic)
            )

            return response.get_data()
        except Exception as e:
            print(e)
            self.status_ok = False

    def set_mode_manual(self):
        try:
            response = SunScreenResponse(
                protocol=self.receive_protocol.set_mode_manual,
                response=self.serial_service.request(self.send_protocol.set_mode_manual)
            )

            return response.get_data()
        except Exception as e:
            print(e)
            self.status_ok = False

    def set_device_name(self, device_name):
        try:
            response = SunScreenResponse(
                protocol=self.receive_protocol.set_device_name,
                response=self.serial_service.request(self.send_protocol.set_device_name, [device_name])
            )

            self.device_name = device_name

            return response.get_data()
        except Exception as e:
            print(e)
            self.status_ok = False
