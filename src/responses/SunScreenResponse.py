class SunScreenResponse:

    response_code: str = 0

    def __init__(self, protocol: dict, response: str):
        self.protocol = protocol
        self.response = response

    def get_data(self):
        response_code, content = self.response.split(':')

        self.response_code = response_code

        data = {}

        if content:
            content = content.split(',')

            i = 0
            for property in self.protocol[int(self.response_code)]:
                data[property['name']] = str(content[i]).replace('\r', '').replace('\n', '').replace(' ', '')
                i = i+1

        return data

    def get_response_code(self):
        return int(self.response_code)