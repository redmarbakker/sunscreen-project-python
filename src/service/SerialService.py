import serial
import time
from typing import List


class SerialService:

    serial = False

    def connect(self, port: str, baudrate: int=9600, timeout: int=5):
        self.serial = serial.Serial(
            port=port,
            baudrate=baudrate,
            timeout=timeout,
        )

        time.sleep(2)

    def disconnect(self):
        self.serial.close()

    def request(self, request_string: str, request_string_data: List[str] = False):
        try:
            if request_string_data is not False:
                request_string = self.parse_request_string(request_string, request_string_data)

            print('request: "' + request_string + '"')

            request_string = request_string + '\r'

            self.serial.writelines([request_string.encode()])
            response = self.serial.readline().decode()
            response = ''.join(e for e in response if e.isprintable()).replace(' ', '')
            print('response: "' + response + '"')

            return response
        except Exception as e:
            print(e)

    def parse_request_string(self, string: str, data: List[str]):
        for value in data:
            string = string.replace('%s', value, 1)

        return string